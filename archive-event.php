<?php get_header(); ?>
<section class="section header-section bg-black">
	<div class="container">
		<h1><?php the_archive_title(); ?></h1>
	</div>
</section>
<section class="section single-service-section">
	<div class="container">
		<div class="main-content tiles">
			<?php

			$args = array(
				'posts_per_page'	=> 9,
				'post_type'			=> 'event',
				'order' => 'DESC',
		    'orderby' => 'meta_value',
		    'meta_key' => 'start_time',
		    'meta_type'			=> 'DATETIME'
			);
			
			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ):
				while ( $the_query->have_posts() ):
					$the_query->the_post();
					include(locate_template('inc/sections/tiles/event-tile.php'));
				endwhile;
				wp_reset_postdata();
			endif;
			?>
			<!-- <div class="pagination">
				<?php echo paginate_links(); ?>
			</div> -->
		</div>
	</div>
</section>
<?php get_footer(); ?>