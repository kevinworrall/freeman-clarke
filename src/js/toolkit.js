(function($) {

  var toolkits;

	function init() {
    setup_toolkits();
    bind_events();
	}

  function bind_events(){
    // Stop form submit if no toolkits selected
    $( '.toolkit-form input[type=submit]' ).click(function() {
      if($('input[name=has-toolkit]').val() < 1){
        $('#no-toolkit').slideDown();
        return false;
      }
      $('#no-toolkit').slideUp();
    });

    // Show thank you message
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      var form_id = event.detail.contactFormId;
      if(form_id === '4841'){
        $('.toolkit-form-container').hide();
        $('.toolkit-complete').show();
        $('html, body').animate({ 
          scrollTop: $(".toolkit-download").offset().top - $('.main-header').outerHeight() - $('#wpadminbar').outerHeight() - 30 
        }, 400);
      }
    }, false );
  }

  function setup_toolkits(){
    toolkits = $('.toolkit-tile');
    if(toolkits.length < 1) return;
    toolkits.each(setup_toolkit);
  }

  function setup_toolkit(){
    var button = $(this).find('.button'),
        id = button.attr('data-toolkit-id'),
        remove_button = $('.selected-toolkits .toolkit-download-item[data-toolkit-id='+id+'] .remove-toolkit'),
        submit = $('button.frm_final_submit');

    submit.prop("disabled", true);
    button.data('data-toolkit-id', id);
    button.on('click', add_toolkit);
    remove_button.on('click', function(){ 
      remove_toolkit(id); 
      return false;
    });
  }

  function add_toolkit(){
    var id = $(this).data('data-toolkit-id'),
        button = $(this),
        checkbox = $('.toolkit-checkboxes #'+id),
        item = $('.selected-toolkits .toolkit-download-item[data-toolkit-id='+id+']'),
        submit = $('button.frm_final_submit');

    if(!id) return;

    // Remove toolkit if already selected
    if(item.hasClass('selected')){
      remove_toolkit(id);
      return false;
    }

    // Update values
    button.text('Added to toolkit').addClass('selected');
    item.addClass('selected');
    checkbox.prop('checked', true);
    $('input[name=has-toolkit]').val(1);
    submit.removeAttr("disabled");
    $('.no-toolkits').hide();
    return false;
  }

  function remove_toolkit(id){
    var button = $('.toolkit-tile .button[data-toolkit-id='+id+']'),
        checkbox = $('.toolkit-checkboxes #'+id),
        item = $('.selected-toolkits .toolkit-download-item[data-toolkit-id='+id+']'),
        submit = $('button.frm_final_submit');

    if(button.length < 1 || item.length < 1) return;
    button.text('Add to toolkit').removeClass('selected');
    item.removeClass('selected');
    checkbox.prop('checked', false);

    if($('.selected-toolkits .toolkit-download-item.selected').length < 1){
      $('input[name=has-toolkit]').val(null);
      submit.prop("disabled", true);
      $('.no-toolkits').show();
    }
  }

  $(init);
})(jQuery);