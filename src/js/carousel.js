(function($) {

	var carousels;

	function init() {
		carousels = $('.carousel');
		carousels.each(setup_carousel);
	}

  function setup_carousel() {
  	var type = $(this).data('type'),
  			options = get_carousel_options(type);
    $(this).owlCarousel(options);
  }

  function get_carousel_options(type) {
  	var options;
  	if(type === 'content'){
  		options = {
        autoplay : true,
        autoplayTimeout : 3500,
        smartSpeed : 1000,
        loop : true,
				items : 1,
				margin : 0,
				nav : true,
				pullDrag : false,
				navText : ['<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"/></svg>'],
			};
  	}
    else if (type === 'logo') {
      options = {
        autoplay : true,
        autoplayTimeout: 4000,
        autoplaySpeed: 500,
        loop : true,
        items : 5,
        margin : 40,
        slideBy: 5,
        nav : true,
        pullDrag : false,
        navText : ['<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"/></svg>'],
        responsive: {
        0: {
           items: 1
        },
        400: {
           items: 2
        },
        544: {
           items: 3
        },
        992: {
           items: 5
        }
     }
      };
    }
  	return options;
  }

  $(init);
})(jQuery);
