(function($) {

	function init() {
    bind_events();
	}

  function bind_events(){

    // Open lightbox
    $('a[data-href]').on('click', function(){
      var url = $(this).attr('data-href');
      $('#field_file').val(url);
      $('.lightbox').fadeIn();
      return false;
    });

    // Close lightbox
    $('.lightbox .close-button').on('click', function(){
      var lightbox = $(this).parents('.lightbox').first();
      lightbox.fadeOut();
      return false;
    });

    // Show thank you message
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      var form_id = event.detail.contactFormId;
      if(form_id === '5232'){
        $('.popup .content').hide();
        $('.popup .visible-only-when-sent').show();
      }
    }, false );
  }

  $(init);
})(jQuery);