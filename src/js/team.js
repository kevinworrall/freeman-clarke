/* global ga */
(function($) {

  var team_members,
      videos = [];

	function init() {
		team_members = $('.team-member');
    if(team_members.length < 1) return;
    team_members.each(setup_team_member);

    $(window).on('resize', set_team_image_height);
    set_team_image_height();
	}

  function set_team_image_height(){
    var images = $('.team-video-container'),
        videos = $('.team-photo'),
        width = images.first().outerWidth();

    images.css('height', width);
    videos.css('height', width);
  }

  function setup_team_member() {
    var container = $(this).find('.team-video-container'),
        button = $(this).find('.content.full > a'),
        video = $(this).find('video'),
        play = $(this).find('.play-button'),
        refresh = $(this).find('.repeat-video'),
        source = '<source src="'+video.attr('data-mp4')+'" type="video/mp4"><source src="'+video.attr('data-mov')+'" type="video/mov">';

    // Setup Videos
    if(video.length){

      // Push the video player to array
      videos.push(video);

      // Play button pressed
      play.on('click',function(){
        $.each( videos, function(){ $(this).get(0).pause(); });
        video.html(source);
        video.load();
        video.get(0).play();
        ga('send', 'event', 'Video', 'Team Video', video.attr('data-mp4'));
        video.on('timeupdate',function(){
          if( this.currentTime > ( this.duration - 3.2 ) )
           container.addClass('video-ended');
        });
        $('.team-video-container').removeClass('video-playing');
        container.addClass('video-playing');
        return false;
      });

      // Refresh button pressed
      refresh.on('click',function(){
        container.removeClass('video-ended').addClass('video-playing');
        video.get(0).pause();
        video.get(0).currentTime = 0;
        video.load();
        video.get(0).play();
        return false;
      });
    }

    // Show more button pressed
    button.on('click', function(){
      var info = $(this).parent(),
          summary = info.find('.summary'),
          full = info.find('.full');

      // Change button text
      if(full.is(':visible')) {
        summary.show();
        full.hide();
        $(this).text('Show more »');
      }else{ 
        full.show();
        summary.hide();
        $(this).text('« Show less');
      }
      return false;
    });
  }

  $(init);
})(jQuery);