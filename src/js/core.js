/* global ga */
(function($) {
  
  function init() {
    setup_navigation();
    setup_service_tiles();
    setup_exit_intent();
    setup_toolkit_download();
    setup_notification_bar();
    setup_bi_video();
    setup_gm_forms();
  }

  function setup_gm_forms(){

    var form = $('form[name=gm_form]');

    form.on('submit', function(){
      var firstName = $(this).find('input[name=FirstName]'),
          lastName = $(this).find('input[name=LastName]'),
          email = $(this).find('input[name=EMailLogin]');

      if(!firstName.val()) firstName.addClass('error');
      else firstName.removeClass('error');

      if(!lastName.val()) lastName.addClass('error');
      else lastName.removeClass('error');

      if(!email.val() || !isEmail(email.val())) email.addClass('error');
      else email.removeClass('error');

      if(firstName.hasClass('error') ||
         lastName.hasClass('error') ||
         email.hasClass('error')){
        return false;
      }
    });
  }

  function setup_bi_video(){

    $('.post-featured-video .play-button').click(function(){
      $('.post-featured-video').addClass('played');
      var vimeoWrap = $('.post-featured-video .video-player');
      vimeoWrap.html( vimeoWrap.html() );

      vimeoWrap.find('iframe').attr('src', vimeoWrap.find('iframe').attr('src') + '&autoplay=true');
      return false;
    });

  }

  function setup_exit_intent(){

    // Bind exit intent
    $.exitIntent('enable');
    $(document).bind('exitintent', fire_exit_intent);

    // Bind close button
    $('.exit-intent-form .top-bar a').on('click', function(){
      $('.exit-intent-forms').fadeOut();
      return false;
    });

    // Bind form completion
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      var form_id = event.detail.contactFormId;
      if(form_id === '4785'){
        $('.exit-intent-form').addClass('completed');
        $.cookie("fc-exit-intent", "true", { path: '/' });
      }
    }, false );
  }

  function fire_exit_intent(){
    var forms = $('.exit-intent-form'),
        rand = Math.floor(Math.random() * forms.length),
        form = forms.eq(rand);

    form.siblings().remove();

    // Check cookie
    if($.cookie("fc-exit-intent") === "true") return;

    // Check page
    if(!$('body').hasClass('page-template-contact') && 
       !$('body').hasClass('page-id-5105') && 
       !$('body').hasClass('blog') &&
       !$('body').hasClass('single-post')) return;

    // Show form and set cookie
    forms.parent().fadeIn();
    form.fadeIn();
    $.cookie("fc-exit-intent", "true", { expires: 10, path: '/' });
  }


  function setup_navigation() {
    var hamburger = $('.hamburger'),
        nav_close_link = $('.main-nav .close-button');

    hamburger.on('click', function(){
      $('body').addClass('nav-open');
      return false;
    });

    nav_close_link.on('click', function(){
      $('body').removeClass('nav-open');
      return false;
    });
  }

  function setup_service_tiles(){
    var service_tiles = $('.tile.service');

    service_tiles.on('click', function(){
      var link = $(this).find('a').first().attr('href');
      window.location.href = link;
      return false;
    });
  }

  function setup_toolkit_download() {
    var button = $('a.trigger-business-insights');

    button.on('click', function(){
      var file = $(this).attr('href');
      ga("send", "event", "Download", "What we do", file);
      return;
    });
  }

  function setup_notification_bar() {
    var notification_bar = $('.notification-bar');
    if(notification_bar.length < 1) return;
    setTimeout(function(){
      notification_bar.slideDown();
    },1000);
  }

  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  $(init);
})(jQuery);
