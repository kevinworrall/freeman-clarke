<?php
function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "" ),
		"singular_name" => __( "Category", "" ),
	);

	$args = array(
		"label" => __( "Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => false,
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "stories_cat",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "stories_cat", array( "stories" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes' );
