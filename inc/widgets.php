<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function fc_widgets_init() {

  $widget_areas = array(
    'Business Insights Sidebar',
    'Client Stories Sidebar',
    'Services Sidebar',
    'Contact Us Sidebar',
    'Page Sidebar',
    'Footer Column 1',
    'Footer Column 2',
    'Footer Column 3',
  );

  foreach ($widget_areas as $widget_area_name) {
    $id = sanitize_title_with_dashes($widget_area_name);
    $id = str_replace( '-', '_', $id );
    register_sidebar(array(
      'name'  => $widget_area_name,
      'id'    => $id,
    ));
  }

}
add_action( 'widgets_init', 'fc_widgets_init' );

/**
 * Unregsiter the default widgets
 */
function fc_unregister_default_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    //unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    // unregister_widget('WP_Nav_Menu_Widget');
    unregister_widget('WP_Widget_Media_Gallery');
    unregister_widget('WP_Widget_Media_Video');
    unregister_widget('WP_Widget_Media_Audio');
    unregister_widget('WP_Widget_Media_Image');
    //unregister_widget('WP_Widget_Custom_HTML');
    unregister_widget('Twenty_Eleven_Ephemera_Widget');
}
add_action('widgets_init', 'fc_unregister_default_widgets', 11);

/**
 * Setup our Widgets
 */
function fc_setup_widgets() {
  register_widget('My_Recent_Posts_Widget');
  register_widget('ClientStories_Widget');
  register_widget('Services_Widget');
}
add_action('widgets_init', 'fc_setup_widgets');

/**
 * Extend Recent Posts Widget 
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 */
Class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts {

  function widget($args, $instance) {
  
    extract( $args );
    
    $title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts') : $instance['title'], $instance, $this->id_base);
        
    if( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
      $number = 10;

    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
    }

    global $displayed_posts;
    if(!$displayed_posts) $displayed_posts = array();
    array_push($displayed_posts, $post_id);
          
    $r = new WP_Query( apply_filters( 'widget_posts_args', array('orderby'=> 'post_date','order' => 'DESC', 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true, 'post__not_in' => $displayed_posts, ) ) );
    if( $r->have_posts() ) :
      ?>
      <div class="business-insights">
        <?php
        if( $title ) echo '<h3>'. $title .'</h3>'; ?>
        <ul>
          <?php while( $r->have_posts() ) : $r->the_post(); ?>        
          <li>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
              <div class="thumbnail" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)">
                <?php
                if(get_field('video')):
                ?>
                <span href="#" class="play-button">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-3 17v-10l9 5.146-9 4.854z"/></svg>
                </span>
                <?php endif; ?>
              </div>
              <span class="title"><?php the_title(); ?></span>
            </a>
          </li>
          <?php endwhile; ?>
        </ul>
        <a href="<?php echo get_site_url(); ?>/business-insights/" class="more-button">See all of our Business Insights ›</a>
       </div>
      <?php   
    wp_reset_postdata();
    
    endif;
  }
}

/**
 * New Widget for showing Client Stories
 */
class ClientStories_Widget extends WP_Widget {

  /**
   * Sets up the widgets name etc
   */
  public function __construct() {
    $widget_ops = array( 
      'classname' => 'clientstories_widget',
      'description' => 'This is a widget for showing client stories',
    );
    parent::__construct( 'clientstories_widget', 'Client Stories', $widget_ops );
  }

  /**
   * Outputs the content of the widget
   *
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
  ?>

  <div class="client-stories">
    <h3>Client stories</h3>
    <ul>
    <?php
    $q_args = array(
      'post_type'      => 'stories',
      'posts_per_page' => 5,
      'order'          => 'ASC',
      'orderby'        => 'rand',
    );

    $query = new WP_Query( $q_args );
    while ($query->have_posts()) {
      $query->the_post();
      ?>
      <li>
        <a href="<?php the_permalink(); ?>">
          <span class="image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></span>
          <span class="content">
            <h4><?php the_title(); ?></h4>
            <?php  
            echo '<p>'.wp_trim_words( get_field('content'), 10, '...' ).'</p>';
            ?>
          </span>
        </a>
      </li>
      <?php
    }
    ?>
    </ul>
    <a href="<?php echo get_site_url(); ?>/client-stories/" class="more-button">See all of our Client Stories ›</a>
  </div>
  <?php
  wp_reset_postdata();
  }
}

/**
 * New Widget for showing Services
 */
class Services_Widget extends WP_Widget {

  /**
   * Sets up the widgets name etc
   */
  public function __construct() {
    $widget_ops = array( 
      'classname' => 'services_widget',
      'description' => 'This is a widget for showing your services',
    );
    parent::__construct( 'services_widget', 'What we do', $widget_ops );
  }

  /**
   * Outputs the content of the widget
   *
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
  ?>

  <div class="services">
    <h3>What we do</h3>
    <ul>
    <?php
    global $post;

    $q_args = array(
      'post_type'      => 'service',
      'posts_per_page' => 5,
      'order'          => 'ASC',
      'orderby'        => 'rand',
      'post__not_in'   => array(get_the_ID()),
    );

    $query = new WP_Query( $q_args );
    while ($query->have_posts()) {
      $query->the_post();
      ?>
      <li>
        <a href="<?php the_permalink(); ?>">
          <span class="icon">
            <?php
            $icon = get_field('icon');
            echo file_get_contents($icon['url']);
            ?>
          </span>
          <span class="content">
            <h4><?php the_title(); ?></h4>
          </span>
        </a>
      </li>
      <?php
    }
    ?>
    </ul>
    <a href="<?php echo get_site_url(); ?>/what-we-do/" class="more-button">See all of our Services ›</a>
  </div>
  <?php
  wp_reset_postdata();
  }
}

