<?php

// function custom_add_form_tag_checkbox() {
// 	//wpcf7_add_form_tag( 'toolkitcheckbox', 'custom_checkbox_form_tag_handler' );
// 	add_shortcode( 'toolkitcheckbox' , 'custom_checkbox_form_tag_handler' );
// }
// add_action( 'wpcf7_init', 'custom_add_form_tag_checkbox' );
 
add_shortcode( 'toolkitcheckbox' , 'custom_checkbox_form_tag_handler' );
function custom_checkbox_form_tag_handler( $tag ) {
	$rows = get_field('toolkits');

	$choices .= '<span class="toolkit-checkboxes">';

	if($rows):
		foreach($rows as $row):
			$title 				= $row['title'];
			$id 					= sanitize_title($title);
			$file 				= $row['file'];
			$file_url 		= $row['file']['url'];
			$file_link 		= '<span style="font-size: 14px; color: #353534;">' . $title . '</span> - <a href="' . $file_url . '" style="font-size: 14px; color: #a2c03b;">Download</a>';
			$file_link 		= htmlspecialchars($file_link);
			$choices .= '<input id="' . $id . '" type="checkbox" required name="toolkit-checkbox[]" value="' . $file_url . '">';
		endforeach;
	endif;

	$choices .= '</span>';
  return $choices;
}


add_shortcode( 'toolkit-checkbox' , 'custom_checkbox_form_tag_handler2' );
function custom_checkbox_form_tag_handler2( $tag ) {
	$array = $_POST['toolkit-checkbox'];
	$html = '<strong>Toolkit File(s):</strong><br/><ul>';

	foreach ($array as $value) {
		$html .= '<li>'.$value.'</li>';
	}

	$html .= '</ul>';

	return $html;
}