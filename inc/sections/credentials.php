<section class="section credentials-section">
	<div class="container">
    <div class="content">
      <h2><?php the_sub_field('title'); ?></h2>
    </div>
    <div class="credentials-section-content">
<?php
  $row = 1;
  if( have_rows('credentials_group') ):
    while ( have_rows('credentials_group') ) : the_row();
?>
      <div class="credential-group credential-row-<?=$row; ?>">
        <h3 class="credential-group-title">
          <?php the_sub_field("title"); ?>
        </h3>
        <div class="credential-group-content">
          <div class="credential-group-image">
            <?php
              $image = get_sub_field('image');
              if( !empty($image) ): ?>
                <style>.credential-row-<?=$row; ?> .credential-group-image { background-image: url(<?php echo $image['url']; ?>); }</style>
              <?php
              endif;
            ?>
          </div>
          <div class="credential-group-list">
            <ul>
              <?php
              if( have_rows('credentials_list') ):
                  while ( have_rows('credentials_list') ) : the_row();
                    echo "<li><span class=\"icon\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"22\" height=\"22\" viewBox=\"0 0 24 24\"><path d=\"M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 13h-5v5h-2v-5h-5v-2h5v-5h2v5h5v2z\"></path></svg></span>". get_sub_field('credential_text') . "</li>";
                  endwhile;
              endif;
              ?>
            </ul>
          </div>
      </div>
    </div>
<?php
      $row ++;
    endwhile;

  endif;
?>
	</div>
  </div>
</section>
