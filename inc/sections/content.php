<section class="section content-section">
	<div class="container">
		<?php 
		if(get_sub_field('content')){
			the_sub_field('content');
		}else { 
			the_content(); 
		}
		?>
	</div>
</section>