<?php
$mp4 = get_field('mp4_video_file');
$mov = get_field('mov_video_file');

$team_email = get_field('email_address');
$team_phone = get_field('phone_number');
$team_linkedin = get_field('linkedin_profile');
?>
<div class="team-video-container">
  <div class="video-container">
    <video controls="" controlslist="nodownload nofullscreen " data-mp4="<?php echo $mp4['url']; ?>" data-mov="<?php echo $mov['url']; ?>">
    </video>
  </div>
  <div class="cover">
    <div class="team-photo" style="background-image: url(<?php echo $image['sizes']['tile-image']; ?>)"></div>
    <a href="#" class="play-button"></a>
  </div>
  <div class="popup">
    <a href="#" class="repeat-video">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M9 12l-4.463 4.969-4.537-4.969h3c0-4.97 4.03-9 9-9 2.395 0 4.565.942 6.179 2.468l-2.004 2.231c-1.081-1.05-2.553-1.699-4.175-1.699-3.309 0-6 2.691-6 6h3zm10.463-4.969l-4.463 4.969h3c0 3.309-2.691 6-6 6-1.623 0-3.094-.65-4.175-1.699l-2.004 2.231c1.613 1.526 3.784 2.468 6.179 2.468 4.97 0 9-4.03 9-9h3l-4.537-4.969z"></path></svg>
    </a>
    <div class="content">
      <?php
      if($team_email) echo '<a class="email" href="mailto:'.$team_email.'">Email</a>';
      if($team_phone) echo '<a class="phone" href="tel:'.$team_phone.'">Call</a>';
      if($team_linkedin) echo '<a class="linkedin" href="'.$team_linkedin.'" target="_BLANK">LinkedIn</a>';
      ?>
    </div>
  </div>
</div>