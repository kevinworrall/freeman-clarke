<?php
$image = get_sub_field('icon');
?>
<header class="section-title">
	<?php if($image): ?>
	<div class="icon">
		<?php echo file_get_contents($image['url']); ?>
	</div>
	<?php endif; ?>
	<h3><?php the_sub_field('title'); ?></h3>
</header>