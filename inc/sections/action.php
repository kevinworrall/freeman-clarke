<?php
unset($has_icon);
$icon = get_sub_field('icon');
if($icon) $has_icon = 'has-icon';
?>
<section class="section action-section <?php echo $has_icon; ?> <?php the_sub_field('id'); ?> <?php fc_section_background_colour(); ?>">
	<div class="container">
		<?php 
		if(get_sub_field('template') !== 'false'):
			get_template_part('inc/sections/action/'.get_sub_field('template'));
		else:
			get_template_part('inc/sections/action/custom');
		endif;
		?>
	</div>
</section>