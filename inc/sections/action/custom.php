<?php
if (have_rows('custom')): 
	while (have_rows('custom')):
		the_row();
		if(get_sub_field('include_title')):
			get_template_part('inc/sections/section-title');
			echo '<div class="content">';
			the_sub_field('content');
			echo '</div>';
		else:
			the_sub_field('content');
		endif;
	endwhile;
endif;
?>