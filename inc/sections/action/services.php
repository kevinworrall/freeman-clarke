<?php
if (have_rows('custom')): 
	while (have_rows('custom')):
		the_row();
		?>
		<div class="services-template">
			<header class="section-title">
				<div class="title">
					<h2>What we do</h2>
				</div>
			</header>
			<div class="content">
				<h3>Our CIOs, CTOs and IT Directors oversee digital innovation, systems and infrastructure.</h3>
			</div>
			<a href="<?php echo get_site_url(); ?>/what-we-do/" class="button">
				Read more
				<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
			</a>
		</div>
		<?php
	endwhile;
endif;
?>