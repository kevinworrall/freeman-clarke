<?php
if (have_rows('custom')): 
	while (have_rows('custom')):
		the_row();
		?>
		<div class="signup-template">
			<header class="section-title">
				<div class="title">
					<h3>Get regular Business Insights direct to your inbox</h3>
				</div>
			</header>
			<div class="content">

				<?php get_template_part('inc/forms/business_insights'); ?>

			</div>
		</div>
		<?php
	endwhile;
endif;
?>