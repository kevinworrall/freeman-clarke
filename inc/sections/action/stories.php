<?php
if (have_rows('custom')): 
	while (have_rows('custom')):
		the_row();
		?>
		<div class="stories-template">
			<header class="section-title">
				<div class="title">
					<h2>Client stories</h2>
				</div>
			</header>
			<div class="content">
				<h3>We’re trusted by our clients to work with them for their long-term business success.</h3>
			</div>
			<a href="<?php echo get_site_url(); ?>/client-stories/" class="button">
				Read their stories
				<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
			</a>
		</div>
		<?php
	endwhile;
endif;
?>