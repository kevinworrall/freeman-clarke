<section class="section column-section">
	<div class="container">
		<div class="col col1of2">
			<?php the_sub_field('left_content'); ?>
		</div>
		<div class="col col1of2">
			<?php the_sub_field('right_content'); ?>
		</div>
	</div>
</section>