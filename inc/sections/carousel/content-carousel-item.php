<?php
$image = get_sub_field('image');
$position = get_sub_field('content_position') . '-align';
?>
<div class="carousel-item <?php echo $position; ?>" style="background-image: url(<?php echo $image['sizes']['large']; ?>)">
	<div class="container">
		<div class="content">
			<div class="v-align">
				<?php the_sub_field('content'); ?>
			</div>
		</div>
	</div>
</div>