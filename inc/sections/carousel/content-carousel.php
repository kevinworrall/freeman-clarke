<div class="carousel content-carousel" data-type="content">
	<?php
	if (have_rows('carousel_items')):
    while (have_rows('carousel_items')):
    	the_row();
    	get_template_part('inc/sections/carousel/content-carousel-item');
    endwhile;
  endif;
	?>
</div>