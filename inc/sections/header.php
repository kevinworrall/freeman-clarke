<section class="section header-section <?php fc_section_background_colour(); ?>">
	<div class="container">
		<h1><?php the_sub_field('title'); ?></h1>
	</div>
</section>