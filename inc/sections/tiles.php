<section class="section tiles-section <?php fc_section_background_colour(); ?>" data-tiles-per-row="<?php the_sub_field('tiles_per_row'); ?>">
	<div class="container">
		<?php
		global $displayed_posts;
		$displayed_posts = array();

		$template = get_sub_field('type');
		if(get_sub_field('include_title')) get_template_part('inc/sections/section-title');
		if(get_sub_field('content')) echo '<div class="content">'.get_sub_field('content').'</div>';
		include(locate_template('inc/sections/tiles/'.$template.'.php'));
		?>
	</div>
</section>
