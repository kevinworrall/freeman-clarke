<?php
$city = do_shortcode('[geoip-city]');
$images = get_sub_field('carousel_images');

if($city == 'London')
	$images = get_sub_field('carousel_images_london');
?>
<section class="section carousel-section">
	<div class="carousel content-carousel" data-type="content" data-location="<?php echo $city; ?>">
		<?php
		if( $images ):
			foreach( $images as $image ):
				?>
				<div class="carousel-item" style="background-image: url(<?php echo $image['sizes']['large']; ?>)">
				</div>
				<?php
			endforeach;
	  endif;
		?>
	</div>

	<div class="container">
		<div class="content-overlay">
			<div class="notice">
				<span>90%</span>
				of our clients rate us top scores for<br/> customer satisfaction
			</div>
			<div class="v-align">
				<?php the_sub_field('content'); ?>
			</div>
		</div>
	</div>
</section>