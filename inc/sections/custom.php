<section class="section custom-section">
	<div class="container">
		<?php $image = get_sub_field('image'); ?>
		<div class="image" style="background-image: url(<?php echo $image['sizes']['large']; ?>)"></div>
		<div class="content">
			<h3><?php the_sub_field('headline'); ?></h3>
			<p><?php the_sub_field('text'); ?></p>
		</div>
		<div class="link">
			<?php
			$link = get_sub_field('button_url');

			if( $link ): 
				$link_url = $link['url'];
				echo do_shortcode('[button url="'.$link_url.'" text="'.get_sub_field('button_text').'"]');
			endif;
			?>
		</div>
	</div>
</section>