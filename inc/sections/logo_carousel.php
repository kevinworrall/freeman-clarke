<section class="section logo-carousel-section<?php if (get_sub_field('remove_bottom_padding')) { echo " remove-bottom-padding"; } ?>">
<div class="container">
  <?php if (get_sub_field('include_a_title')): ?>
  <div class="content">
    <h2><?php the_sub_field('title'); ?></h2>
  </div>
<?php endif; ?>
	<div class="carousel content-carousel" data-type="logo" data-location="<?php echo $city; ?>">
		<?php
    if( have_rows('logo_carousel_content') ):
        while ( have_rows('logo_carousel_content') ) : the_row();
          $logo_image = get_sub_field('image');
          if( !empty($logo_image) ):

            if (get_sub_field('optional_link')) {
              echo '<a href="' . get_sub_field('optional_link') . '">';
            }
          ?>
            <img src="<?php echo $logo_image['url']; ?>" alt="<?php echo $logo_image['alt']; ?>" class=""/>
          <?php
          if (get_sub_field('optional_link')) {
            echo '</a>'; 
          }
          endif;
        endwhile;
    endif;
		?>
	</div>
</div>
</section>
