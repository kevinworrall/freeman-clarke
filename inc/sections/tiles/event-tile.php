<?php
$type = "Event";
unset($class);
$date_now = date('Y-m-d H:i:s');
$format_in = 'jS F Y - g:i a';
$format_out = 'Y-m-d H:i:s';
$date_field = DateTime::createFromFormat($format_in, get_field('start_time'));
if($date_now > $date_field->format( $format_out )) {
	$class = "expired";
	$type = "This event has expired";
}

$image_url = get_the_post_thumbnail_url(get_the_ID(),'tile-image');
?>
<div class="event tile <?php echo $class; ?>">
	<a href="<?php the_permalink(); ?>" class="image" style="background-image: url(<?php echo $image_url; ?>)"></a>
	<div class="content">
		<h4 class="title"><?php the_title(); ?></h4>
		<p>
			<?php 
			echo '<strong>'.get_field('start_time').'</strong>';
			if(get_field('end_time')) echo ' until <strong>'.get_field('end_time').'</strong>';
			?>
		</p>
		<p>
		<?php 
		if(get_field('short_description')){
			echo wp_trim_words( get_field('short_description'), 20, '...' );
		}else{
			echo wp_trim_words( get_the_content(), 20, '...' );
		}
		?>
		</p>
		<a href="<?php the_permalink(); ?>">Read more</a>
		<div class="content-type"><?php echo $type; ?></div>
	</div>
</div>