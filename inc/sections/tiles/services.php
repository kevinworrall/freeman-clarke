<div class="services tiles">
	<?php
	$posts = get_sub_field('services');
	$i=0;
	if ( $posts ):
		foreach ( $posts as $post ):
			$i++;
			setup_postdata($post); 
			if($i==5) include(locate_template('inc/sections/tiles/services-text.php'));
			include(locate_template('inc/sections/tiles/services-tile.php'));
		endforeach;
		wp_reset_postdata();
	endif;
	?>
</div>