<?php if(get_sub_field('show_more_button')): ?>
<a href="<?php echo get_site_url(); ?>/client-stories/" class="button">
	View all client stories
	<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
</a>
<?php endif; ?>
<div class="client-story tiles">
	<?php
	if(get_sub_field('show_latest')){
		$args = array(
			'posts_per_page'   => get_sub_field('count'),
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => 'stories',
			'post_status'      => 'publish',
		);
		$posts = get_posts($args);
	}else{
		$posts = get_sub_field('client_stories');
	}

	if ( $posts ):
		foreach ( $posts as $post ):
			setup_postdata($post); 
			include(locate_template('inc/sections/tiles/client-stories-tile.php'));
		endforeach;
		wp_reset_postdata();
	endif;
	?>
</div>