<?php
global $displayed_posts;
array_push($displayed_posts, get_the_ID());

$image_url = get_the_post_thumbnail_url(get_the_ID(),'tile-image');
$video = get_field('video');

unset($class);
if($video) $class = 'video-tile';
?>
<div class="business-insight tile <?php echo $class; ?>">
	<a href="<?php the_permalink(); ?>" class="image" style="background-image: url(<?php echo $image_url; ?>)">
		<?php if($video): ?>
			<span href="#" class="play-button">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-3 17v-10l9 5.146-9 4.854z"/></svg>
			</span>
		<?php endif; ?>
	</a>
	<div class="content">
		<h4 class="title"><?php the_title(); ?></h4>
		<p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
		<a href="<?php the_permalink(); ?>">Read more</a>
		<div class="content-type">Business Insight</div>
	</div>
</div>