<?php 
unset($featured);
if(get_field('featured'))
 $featured = ' featured';
?>
<div class="service tile<?php echo $featured; ?>">
	<div class="content">
		<div class="icon">
			<?php
			$icon = get_field('icon');
			if($icon){
				echo file_get_contents($icon['url']);
			}
			?>
		</div>
		<h3 class="title"><?php the_title(); ?></h3>
		<p><?php the_field('description'); ?>
		<a href="<?php the_permalink(); ?>">Read more</a>
		<div class="content-type">Service</div>
	</div>
</div>
