<?php
$id = sanitize_title(get_sub_field('title'));
$image = get_sub_field('image');
?>
<div class="tile toolkit-tile">
	<header>
		<div class="toolkit-image">
			<img src="<?php echo $image['sizes']['large']; ?>"/>
		</div>
		<div class="toolkit-title">
			<?php the_sub_field('title'); ?>
		</div>
	</header>
	<article class="toolkit-desc">
		<?php the_sub_field('desc'); ?>
		<a href="#" class="button" data-toolkit-id="<?php echo $id; ?>">Add to toolkit</a>
	</article>
</div>