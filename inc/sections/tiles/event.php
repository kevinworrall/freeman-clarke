<?php if(get_sub_field('show_more_button')): ?>
<a href="<?php echo get_site_url(); ?>/events/" class="button">
	See all our events
	<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
</a>
<?php endif; ?>
<div class="events tiles">
	<?php
	$posts = get_sub_field('events');
	if ( $posts ):
		foreach ( $posts as $post ):
			setup_postdata($post); 
			include(locate_template('inc/sections/tiles/event-tile.php'));
		endforeach;
		wp_reset_postdata();
	endif;
	?>
</div>