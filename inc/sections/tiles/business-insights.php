<?php if(get_sub_field('show_more_button')): ?>
<a href="<?php echo get_site_url(); ?>/business-insights/" class="button">
	Read more business insights
	<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
</a>
<?php endif; ?>
<div class="business-insights tiles">
	<?php
	if(get_sub_field('show_latest')){
		$args = array(
			'posts_per_page'   => get_sub_field('count'),
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
		);
		$posts = get_posts($args);
	}else{
		$posts = get_sub_field('business_insights');
	}

	if ( $posts ):
		foreach ( $posts as $post ):
			setup_postdata($post); 
			include(locate_template('inc/sections/tiles/business-insights-tile.php'));
		endforeach;
		wp_reset_postdata();
	endif;
	?>
</div>