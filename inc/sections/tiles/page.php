<div class="pages tiles">
	<?php
	$posts = get_sub_field('pages');
	if ( $posts ):
		foreach ( $posts as $post ):
			setup_postdata($post); 
			include(locate_template('inc/sections/tiles/page-tile.php'));
		endforeach;
		wp_reset_postdata();
	endif;
	?>
</div>