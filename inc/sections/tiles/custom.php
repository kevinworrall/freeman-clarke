<div class="tiles">
<?php
if ( have_rows('custom_tiles') ):
  while ( have_rows('custom_tiles') ):
  	the_row();
  	
  	$post_object = get_sub_field('tile_content');

		if( $post_object ): 
			$post = $post_object;
			setup_postdata( $post ); 
			$post_type = get_post_type();
			if($post_type == 'post') $post_type = 'business-insights';
			if($post_type == 'stories') $post_type = 'client-stories';
			if($post_type == 'service') $post_type = 'services';
			include(locate_template('inc/sections/tiles/'.$post_type.'-tile.php'));
		endif;

		wp_reset_postdata();
  endwhile;
endif;

// if ( $posts ):
// 	foreach ( $posts as $post ):
// 		setup_postdata($post); 
// 		the_title();
// 		//include(locate_template('inc/sections/tiles/page-tile.php'));
// 	endforeach;
// 	wp_reset_postdata();
// endif;
?>
</div>