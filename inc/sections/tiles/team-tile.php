<div class="team-member tile" id="<?php echo $post->post_name; ?>">
	<?php
	$image = get_field('photo');
	$position = get_field('position');
	$location = get_field('location');
	$button_text = 'Read more';

	if(get_field('include_video')):
		include(locate_template('inc/sections/team/video.php'));
	else:
		include(locate_template('inc/sections/team/photo.php'));
	endif;
	?>
	<div class="content<?php if($show_info) echo ' full'; ?>">
		<h4 class="title"><?php the_title(); ?></h4>
		<p class="position"><?php echo $position ?></p>
		<?php 
		if($location)
			echo '<small class="location">'.$location.'</small>';

		if($show_info):
			$button_text = 'Show more »';
			$full = get_field('biography');
			$summary = wp_trim_words( $full, 20, '...' );
		?>
			<div class="team-member-info">
				<div class="summary"><p><?php echo $summary; ?></p></div>
				<div class="full"><?php echo $full; ?></div>
			</div>
		<?php
		endif;
		?>
		<a href="<?php echo get_site_url(); ?>/our-team/#<?php echo $post->post_name ?>"><?php echo $button_text; ?></a>
		<div class="content-type">Team member</div>
	</div>
	<div class="social-icons">
		<?php if(get_field('linkedin_profile')): ?>
			<a target="_BLANK" href="<?php the_field('linkedin_profile'); ?>" class="linkedin">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0v24h24v-24h-24zm8 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.397-2.586 7-2.777 7 2.476v6.759z"></path></svg>
			</a>
		<?php endif; ?>
		<?php if(get_field('email_address')): ?>
			<a href="mailto:<?php the_field('email_address'); ?>" class="email">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm6.623 7.929l-4.623 5.712v-9.458l4.623 3.746zm-4.141-5.929h19.035l-9.517 7.713-9.518-7.713zm5.694 7.188l3.824 3.099 3.83-3.104 5.612 6.817h-18.779l5.513-6.812zm9.208-1.264l4.616-3.741v9.348l-4.616-5.607z"></path></svg>
			</a>
		<?php endif; ?>
	</div>
</div>