<?php
$image_url = get_the_post_thumbnail_url(get_the_ID(),'tile-image');
?>
<div class="event tile">
	<?php if($image_url): ?>
	<a href="<?php the_permalink(); ?>" class="image" style="background-image: url(<?php echo $image_url; ?>)"></a>
	<?php endif; ?>
	<div class="content">
		<h4 class="title"><?php the_title(); ?></h4>
		<?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?>
		<a href="<?php the_permalink(); ?>">Read more</a>
		<div class="content-type">Page</div>
	</div>
</div>