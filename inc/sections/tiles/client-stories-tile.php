<?php
$image_url = get_the_post_thumbnail_url(get_the_ID(),'tile-image');
?>
<div class="client-story tile">
	<a href="<?php the_permalink(); ?>" class="image" style="background-image: url(<?php echo $image_url; ?>)"></a>
	<div class="content">
		<h4 class="title"><?php the_title(); ?></h4>
		<p><?php the_field('description'); ?>
		<a href="<?php the_permalink(); ?>">Read more</a>
		<div class="content-type">Client story</div>
	</div>
</div>