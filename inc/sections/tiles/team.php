<?php if(get_sub_field('show_more_button')): ?>
<a href="<?php echo get_site_url(); ?>/our-team/" class="button">
	Meet our team
	<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
</a>
<?php endif; ?>
<div class="team-members tiles">
	<?php
	$posts = get_sub_field('team_members');
	$show_info = get_sub_field('team_member_info');
	if ( $posts ):
		foreach ( $posts as $post ):
			setup_postdata($post);
			include(locate_template('inc/sections/tiles/team-tile.php'));
		endforeach;
		wp_reset_postdata();
	endif;
	?>
</div>