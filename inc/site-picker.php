<nav class="site-picker">
	<?php

	$flags = array(
		1 => 'https://www.freemanclarke.co.uk/wp-content/uploads/2018/08/uk-flag.svg',
		2 => 'https://www.freemanclarke.com/wp-content/uploads/sites/2/2018/10/US-flag-35x24-circles.png',
		4 => 'https://www.freemanclarke.sg/wp-content/uploads/sites/4/2018/09/singaporean-flag.svg'
	);

	$sites = get_sites();
	$current_site_id = get_current_blog_id();
  foreach ( $sites as $site ):
    switch_to_blog( $site->blog_id );
		$site_flag = $flags[$site->blog_id];
		$site_url = 'https://'.$site->domain;

		if($current_site_id == $site->blog_id) $classes = ' class="active"';
		echo '<a href="'.$site_url.'"'.$classes.' style="background-image: url('.$site_flag.')"></a>';
    restore_current_blog();
    unset($classes);
  endforeach;
	?>
</nav>