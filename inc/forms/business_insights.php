<?php
$site_id = get_current_blog_id();
$form_id;

if($site_id == 1) $form_id = 811;
if($site_id == 2) $form_id = 816;
if($site_id == 4) $form_id = 817;
?>
<form id='gm_form_<?= $form_id; ?>' name='gm_form' method='post' action='https://www.communigatormail.co.uk/fdc_freemanclarkelz//gapi/form/postform/<?= $form_id; ?>?instanceName=fdc_freemanclarkelz'
      data-gmform="gm_form_<?= $form_id; ?>" data-gminstance="fdc_freemanclarkelz" data-gmurl="http://freeman-clarke.co.uk/fdc_freemanclarkelz/" data-gmrecaptchaenabled="False"
      data-gmrecaptchaid="" data-gmname="Business Insights Newsletter Sign Up UK" data-gmrecaptcha="6LeVySQTAAAAAMnCZcXEiaQuefOEVlKCywJU5rN5" data-gmregex=""
      data-gmisvalidationenabled="False" data-gmregexmessage="" data-gmcallback="gm_callback_<?= $form_id; ?>" data-gmpopulate="False">
        <div>
            <input placeholder="First name" name='FirstName' id="FirstName" data-mandatory='False' value="" data-populate='False' type='Text' />
        </div>
        <div>
            <input placeholder="Last name" name='LastName' id="LastName" data-mandatory='False' value="" data-populate='False' type='Text' />
        </div>
        <div>
            <input placeholder="Business email address" name='EMailLogin' id="EMailLogin" data-mandatory='True' value="" data-populate='False' type='Text' />
                    <div name='mandatory_EMailLogin' style='display:none; color:#ff0000'>
            * Please enter an email address
            
        </div>
        </div>
    <div style="display: none">
        <input name='new_newsletterrecipient' id='new_newsletterrecipient'  data-mandatory='False' data-populate='True' checked type='checkbox' value="true" />newnewsletterrecipient
    </div>
    <div style="display: none">
        <label for='New_Country'>NewCountry</label>
    </div>
    <div style="display: none">
        <select name='New_Country' data-mandatory='False' data-populate='True' id='New_Country'>
                <option value='United Kingdom' selected>United Kingdom</option>
             <option value='USA'>USA</option>
             <option value='Singapore'>Singapore</option>
        </select>
    </div>
            <input name='DataSource' id='DataSource' value='BI Sign Up' data-populate='False' type='hidden' />
    
    <div id="<?= $form_id; ?>_enter" style="display: none;">
        <!-- Do not change this field. If changed the form becomes invalid.-->
        <input type="text" name="<?= $form_id; ?>_email" autocomplete="off" value="email@email.com" />
    </div>
    <div id="<?= $form_id; ?>_onenter" style="display: none;">
        <!-- Please leave this field blank. If populated the form becomes invalid. -->
        <input type="text" name="<?= $form_id; ?>_onemail" autocomplete="off" value="" />
    </div>
    <div>
        <input type="submit" value="Submit" />
    </div>
</form>

<script type="text/javascript">

    function validateForm_gm_form_<?= $form_id; ?>() {
        
        

        _gmf.trackSubmissionGatorLeads("gm_form_<?= $form_id; ?>");
        return _gmf.validateForm("gm_form_<?= $form_id; ?>")
    }

    function gm_callback_<?= $form_id; ?>(){
    
    };

</script>