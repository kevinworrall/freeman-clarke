<?php 
/* Template Name: Contact Page */
get_header(); 
if (have_posts()):
	while (have_posts()):
		the_post();
		?>

<section class="section header-section bg-black">
	<div class="container">
		<h1><?php the_title(); ?></h1>
	</div>
</section>

<section class="section contact-section">
	<div class="container">
		<div class="main-content">
			<?php the_content(); ?>
		</div>
		<aside class="sidebar">
			<h3>Contact information</h3>
			<div class="contact-details">
				<?php if(get_field('site_phone_number', 'option')): ?>
				<div class="contact-detail">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M18.48 22.926l-1.193.658c-6.979 3.621-19.082-17.494-12.279-21.484l1.145-.637 3.714 6.467-1.139.632c-2.067 1.245 2.76 9.707 4.879 8.545l1.162-.642 3.711 6.461zm-9.808-22.926l-1.68.975 3.714 6.466 1.681-.975-3.715-6.466zm8.613 14.997l-1.68.975 3.714 6.467 1.681-.975-3.715-6.467z"/></svg>
					<div class="content">
						<p><strong>Phone number</strong></p>
						<p><a href="tel:<?php the_field('site_phone_number', 'option'); ?>"><?php the_field('site_phone_number', 'option'); ?></a></p>
					</div>
				</div>
				<?php endif; ?>
				<div class="contact-detail">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12.713l-11.985-9.713h23.971l-11.986 9.713zm-5.425-1.822l-6.575-5.329v12.501l6.575-7.172zm10.85 0l6.575 7.172v-12.501l-6.575 5.329zm-1.557 1.261l-3.868 3.135-3.868-3.135-8.11 8.848h23.956l-8.11-8.848z"/></svg>
					<div class="content">
						<p><strong>Email</strong></p>
						<p><a href="mailto:<?php the_field('site_email_address', 'option'); ?>"><?php the_field('site_email_address', 'option'); ?></a></p>
					</div>
				</div>
				<div class="contact-detail">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 6.453l9 8.375v9.172h-6v-6h-6v6h-6v-9.172l9-8.375zm12 5.695l-12-11.148-12 11.133 1.361 1.465 10.639-9.868 10.639 9.883 1.361-1.465z"/></svg>
					<div class="content">
						<p><strong>Head office</strong></p>
						<p><?php the_field('head_office_address','option'); ?></p>
					</div>
				</div>
				<div class="contact-detail">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
					<div class="content">
						<p><strong>Locations</strong></p>
						<p>
							<?php
    					if ( is_active_sidebar( 'footer_column_2' ) ):
      					dynamic_sidebar( 'footer_column_2' );
      				endif;
							?>
						</p>
					</div>
				</div>
			</div>
		</aside>
	</div>
</section>

		<?php
	endwhile;
endif;
get_footer();
?>