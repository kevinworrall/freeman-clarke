	<div class="exit-intent-forms">

		<div class="exit-intent-form" style="background-image: url(<?php theme_image('exit-intent1.jpg'); ?>)">
			<div class="top-bar">
				<a href="#">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="70px" height="70px" viewBox="0 0 70 70" enable-background="new 0 0 70 70" xml:space="preserve">
					<path d="M34.999,1.739c-18.369,0-33.26,14.892-33.26,33.26c0,18.369,14.891,33.262,33.26,33.262s33.262-14.893,33.262-33.262
						C68.261,16.631,53.368,1.739,34.999,1.739z M50.833,38.858c-3.855,0-7.711,0-11.568,0c0,3.969,0,7.938,0,11.906
						c0,5.373-8.331,5.373-8.331,0c0-3.969,0-7.938,0-11.906c-3.969,0-7.938,0-11.907,0c-5.372,0-5.372-8.33,0-8.33
						c3.97,0,7.938,0,11.907,0c0-3.857,0-7.715,0-11.57c0-5.373,8.331-5.373,8.331,0c0,3.855,0,7.713,0,11.57c3.857,0,7.713,0,11.568,0
						C56.206,30.528,56.206,38.858,50.833,38.858z"/>
					</svg>
				</a>
			</div>
			<div class="content left">
				<h2>Subscribe to our Business Insights</h2>
				<p>Plain English board-level briefings focused on technology strategies to deliver competitive advantage and business success.</p>
				<?php get_template_part('inc/forms/business_insights'); ?>
				<p>You can unsubscribe at any time.</p>
			</div>
			<div class="content left complete">
				<h2>Thank you.</h2>
				<h2>You’ll now receive regular expert business insights.</h2>
				<p>Call us on 0203 020 1864 with any questions.</p>
			</div>
			<div class="tab">
				<p>Graeme Freeman<br/>
					Co-Founder and Director
				</p>
			</div>
		</div>

		<div class="exit-intent-form alternative" style="background-image: url(<?php theme_image('exit-intent2.jpg'); ?>)">
			<div class="top-bar">
				<a href="#">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="70px" height="70px" viewBox="0 0 70 70" enable-background="new 0 0 70 70" xml:space="preserve">
					<path d="M34.999,1.739c-18.369,0-33.26,14.892-33.26,33.26c0,18.369,14.891,33.262,33.26,33.262s33.262-14.893,33.262-33.262
						C68.261,16.631,53.368,1.739,34.999,1.739z M50.833,38.858c-3.855,0-7.711,0-11.568,0c0,3.969,0,7.938,0,11.906
						c0,5.373-8.331,5.373-8.331,0c0-3.969,0-7.938,0-11.906c-3.969,0-7.938,0-11.907,0c-5.372,0-5.372-8.33,0-8.33
						c3.97,0,7.938,0,11.907,0c0-3.857,0-7.715,0-11.57c0-5.373,8.331-5.373,8.331,0c0,3.855,0,7.713,0,11.57c3.857,0,7.713,0,11.568,0
						C56.206,30.528,56.206,38.858,50.833,38.858z"/>
					</svg>
				</a>
			</div>
			<div class="content full">
				<h2>Subscribe to our Business Insights</h2>
			</div>
			<div class="content right">
				<p>Plain English board-level briefings focused on technology strategies to deliver competitive advantage and business success.</p>
				<?php get_template_part('inc/forms/business_insights'); ?>
				<p>You can unsubscribe at any time.</p>
			</div>
			<div class="content right complete">
				<h2>Thank you.</h2>
				<h2>You’ll now receive regular expert business insights.</h2>
				<p>Call us on 0203 020 1864 with any questions.</p>
			</div>
		</div>

	</div>

	<footer class="main-footer">
		<div class="container">
			<div class="col footer-logo">
				<a href="<?php echo site_url(); ?>" class="main-logo">
					<img src="<?php theme_image('fc-logo.png'); ?>"/>
				</a>
			</div>
			<?php fc_footer_widgets(); ?>
			<div class="footer-bottom">
				<?php
				$m = array(
					1 => 'Freeman and Clarke Limited',
					2 => 'Freeman and Clarke Inc',
					4 => 'Freeman and Clarke PTE'
				);
				?>
				<p><a title="Version 2.0">Copyright © <?php echo date("Y") .' '. $m[get_current_blog_id()]; ?></a></p>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	<script data-cfasync='false' type='text/javascript' defer='' async='' src='https://t.gatorleads.co.uk/Scripts/ssl/e87dbf54-1269-455d-b853-04317587c57a.js'></script>
</body>
</html>