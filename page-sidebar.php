<?php 
get_header();
?>
<section class="section header-section bg-black">
	<div class="container">
		<h1><?php the_title(); ?></h1>
	</div>
</section>

<section class="section single-service-section tiles-section" data-tiles-per-row="2">
	<div class="container">
		<div class="main-content tiles">
			<?php
			if (have_posts()):
				while (have_posts()):
					the_post();

					if (have_rows('sections')):
		        while (have_rows('sections')):
		          the_row();

		          // Get Custom Tiles
		          if ( have_rows('custom_tiles') ):
							  while ( have_rows('custom_tiles') ):
							  	the_row();
							  	
							  	$post_object = get_sub_field('tile_content');

									if( $post_object ): 
										$post = $post_object;
										setup_postdata( $post ); 
										$post_type = get_post_type();
										if($post_type == 'post') $post_type = 'business-insights';
										if($post_type == 'stories') $post_type = 'client-stories';
										if($post_type == 'service') $post_type = 'services';
										include(locate_template('inc/sections/tiles/'.$post_type.'-tile.php'));
									endif;

									wp_reset_postdata();
							  endwhile;
							endif;


		        endwhile;
		      endif;

				endwhile;
			endif;
			?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>
<?php get_footer(); ?>