<?php 
get_header(); 
get_sections();
?>
<?php 
if($_GET['utm_source'] !== 'GatorMail'): ?>
	<div class="lightbox">
		<div class="popup" data-popup="pdf-download">
			<a href="#" class="close-button">
				<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 24 24"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg>
			</a>
			<header class="popup-header">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 13h-5v5h-2v-5h-5v-2h5v-5h2v5h5v2z"></path></svg>
				<h2>Business insights</h2>
			</header>
			<div class="content">
				<?php echo do_shortcode('[formidable id=pkzvj]'); ?>
			</div>
			<div class="visible-only-when-sent">
				<h3>Thank you.</h3>
				<h4>We’ve sent an email to the address you provided. Please open your email and click on the download link.</h4>
				If you can’t see an email from Freeman Clarke in your inbox please check your spam folder or get in touch at <a href="tel:02030201864">0203&nbsp;020&nbsp;1864.</a>
			</div>
		</div>
	</div>
	<?php 
endif;
get_footer();
?>