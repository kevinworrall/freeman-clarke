<aside class="sidebar">
	<?php 

	$parent_template = get_page_template_slug($post->post_parent);
	if($parent_template == 'page_campaign.php'):
		the_widget('My_Recent_Posts_Widget',array('title' => 'Business insights', 'number' => 5));
	else:
		if(is_active_sidebar( 'page_sidebar' )){
			dynamic_sidebar( 'page_sidebar' );
		}

	$parent_post = get_post($post->post_parent);
	$parent_post_title = $parent_post->post_title;
	$current_page_id = get_the_ID();
	?>

	<div class="widget subpage-widget">
		<h2 class="widgettitle"><?php echo $parent_post_title; ?></h2>
		<ul>

			<?php
			$args = array(
		    'post_type'      => 'page',
		    'posts_per_page' => -1,
		    'post_parent'    => $post->post_parent,
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order'
			);

			$parent = new WP_Query( $args );
			if ( $parent->have_posts() ) : 
				while ( $parent->have_posts() ) : 
					$parent->the_post(); 
					if($current_page_id == get_the_ID()) echo '<li><a href="'.get_the_permalink().'"><strong>'.get_the_title().'</strong></a></li>';
					else echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
				endwhile;
			endif;
			?>
		</ul>
	</div>
	<?php endif; ?>
</aside>