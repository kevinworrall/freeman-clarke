<?php /* Template Name: Campaign Page */ ?>
<?php get_header(); ?>

<section class="campaign-section">
	<div class="container">

		<div class="main-content">
			<?php the_field('main_content'); ?>
			<?php
			if(get_field('form_shortcode')):
				echo do_shortcode(get_field('form_shortcode'));
			else:
				the_field('gator_form_js');
				the_field('gator_form_html');
			endif;
			?>
		</div>

		<aside class="sidebar">
			<?php
			$file = get_field('file');
			?>
			<input type="hidden" name="campaign-file" value="<?php echo $file['url']; ?>"/>
			<h3><?php the_field('file_name'); ?></h3>
			<h4>By <?php the_field('file_author'); ?></h4>
			<?php
			$image = get_field('file_image');
			if( !empty($image) ): ?>
				<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</aside>

	</div>
</section>

<?php get_footer(); ?>