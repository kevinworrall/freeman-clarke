<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
<?php wp_head(); ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KM785CK');</script>
<!-- End Google Tag Manager -->
<!-- Gator tracking code -->
<!-- <script name='form_gm' src="https://www.communigatormail.co.uk/fdc_freemanclarkelz//Form/gf_t.min.js" async defer></script> -->
<script data-cfasync='false' type='text/javascript' defer='' async='' src='https://t.wowanalytics.co.uk/Scripts/ssl/34a0bd7b-166f-4b23-80c5-d6f7ba716140.js'></script>
<!-- End Gator tracking code -->

<script name='form_gm' src="https://www.communigatormail.co.uk/fdc_freemanclarkelz//Form/gf_t.min.js" async defer></script>

</head>
<body <?php body_class(); ?>>	
	
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KM785CK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="main-header">
	<?php get_template_part('inc/notification','bar'); ?>
	<aside class="top-bar">
		<div class="container">
			<div class="col">
				<p><?php bloginfo('description'); ?></p>
			</div>

			<div class="col">
				<?php if(get_field('site_phone_number', 'option')): ?>
				<a href="tel: <?php the_field('site_phone_number', 'option'); ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M18.48 22.926l-1.193.658c-6.979 3.621-19.082-17.494-12.279-21.484l1.145-.637 3.714 6.467-1.139.632c-2.067 1.245 2.76 9.707 4.879 8.545l1.162-.642 3.711 6.461zm-9.808-22.926l-1.68.975 3.714 6.466 1.681-.975-3.715-6.466zm8.613 14.997l-1.68.975 3.714 6.467 1.681-.975-3.715-6.467z"/></svg>
					<?php the_field('site_phone_number', 'option'); ?>
				</a>
				<?php endif; ?>

				<a href="mailto: <?php the_field('site_email_address', 'option'); ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12.713l-11.985-9.713h23.971l-11.986 9.713zm-5.425-1.822l-6.575-5.329v12.501l6.575-7.172zm10.85 0l6.575 7.172v-12.501l-6.575 5.329zm-1.557 1.261l-3.868 3.135-3.868-3.135-8.11 8.848h23.956l-8.11-8.848z"/></svg>
					<?php the_field('site_email_address', 'option'); ?>
				</a>
				<?php get_template_part('inc/site-picker'); ?>
			</div>
		</div>
	</aside>
	<section class="header-content container">
		<a href="<?php echo site_url(); ?>" class="main-logo">
			<img src="<?php theme_image('fc-logo.png'); ?>"/>
		</a>
		<div class="hamburger-container">
			<button class="hamburger hamburger--spin" type="button">
			  <span class="hamburger-box">
			    <span class="hamburger-inner"></span>
			  </span>
			</button>
		</div>
		<nav class="main-search">
			<?php get_search_form(); ?>
		</nav>
		<nav class="main-nav">
			<a href="#" class="close-button">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.538l-4.592-4.548 4.546-4.587-1.416-1.403-4.545 4.589-4.588-4.543-1.405 1.405 4.593 4.552-4.547 4.592 1.405 1.405 4.555-4.596 4.591 4.55 1.403-1.416z"/></svg>
			</a>
			<?php wp_nav_menu( array( 'theme_location' => 'main-navigation', 'container' => false ) ); ?>
		</nav>
	</section>
</header>