<?php get_header(); ?>
<section class="section header-section bg-black">
	<div class="container">
		<h1>
			<?php
			$post = get_queried_object();
			$postType = get_post_type_object(get_post_type($post));
			if ($postType) {
			    echo esc_html($postType->labels->name);
			}else{
				echo 'Client stories';
			}
			?>
		</h1>
	</div>
</section>
<section class="section client-stories-section">
	<div class="container">
		<div class="main-content">
			<h1><?php the_title(); ?></h1>
			<?php
			the_post_thumbnail( 'large', $attr );
			$logo = get_field('client_logo');
			echo '<img src="'.$logo['sizes']['large'].'" alt="'.$logo['alt'].'" title="'.$logo['title'].'"/>';
			the_field('content');
			?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>
<?php get_footer(); ?>