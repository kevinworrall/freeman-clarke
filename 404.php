<?php 
get_header();
header("HTTP/1.1 301 Moved Permanently");
header("Location: ".get_bloginfo('url'));
exit();
?>
<section class="section header-section bg-black">
	<div class="container">
		<h1>404 - Page not found</h1>
	</div>
</section>
<div class="section">
	<div class="container">
		<h3 style="text-align: center">Sorry but the page you were looking for cannot be found.</h3>
	</div>
</div>
<?php get_footer(); ?>