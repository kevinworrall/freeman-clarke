<?php /* Template Name: Toolkit Page */ ?>
<?php get_header(); ?>

<section class="section tiles-section toolkit-section">
	<div class="container">
		<header class="section-title">
			<div class="icon icon-large">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 13h-5v5h-2v-5h-5v-2h5v-5h2v5h5v2z"></path></svg>	</div>
			<h1><?php the_title(); ?></h1>
		</header>

		<div class="content">
			<p><?php the_field('summary_text'); ?></p>
		</div>

		<?php
		if( have_rows('toolkits') ):
			echo '<div class="tiles toolkit-tiles">';
   		while ( have_rows('toolkits') ):
   			the_row();
   			get_template_part('inc/sections/tiles/toolkit','tile');
   		endwhile;

   		if(is_page('digital-transformation-toolkit')):
			?>

				<div class="tile toolkit-tile dark-tile">
					<h2>Diagnostic Tool for Scale Ups</h2>
					<p>Quick to complete, report and helpful actions, currently f-o-c.</p>
					<a href="http://www.scaleupdiagnostic.co.uk" target="_BLANK" class="button">See Diagnostic Tool</a>
					<div>
						<img src="https://www.freemanclarke.co.uk/wp-content/uploads/2018/09/DT-Vistage-logo.jpg" class="logo">
						<img src="https://www.freemanclarke.co.uk/wp-content/uploads/2018/09/DT-FC-Centre-logo.jpg" class="logo">
						<img src="https://www.freemanclarke.co.uk/wp-content/uploads/2018/09/DT-FC-logo.jpg" class="logo">
						<img src="https://www.freemanclarke.co.uk/wp-content/uploads/2018/09/DT-Smith-Williamson-logo.jpg" class="logo">
					</div>
				</div>

			<?php
			endif;

   		echo '</div>';
   	endif;
		?>

		<div class="toolkit-download">
			<header>
				<h3>Your <?php the_title(); ?></h3>
			</header>
			<article class="toolkit-form-container">
				<h3>Download your <?php the_title(); ?>.</h3>
				<div class="col col1of2 toolkit-downloads">
					<h4>You’ve selected the following CEO Briefings:</h4>
					<div class="selected-toolkits">

						<p class="no-toolkits" style="display: block; font-size: 13px; line-height: 19px; font-weight: 600; color: #3d8099; max-width: 60%; margin: auto;">Please select the CEO briefings from above that you'd like to be included in your toolkit.</p>

						<?php
						if( have_rows('toolkits') ):
				   		while ( have_rows('toolkits') ):
				   			the_row();
				   			get_template_part('inc/sections/tiles/toolkit','download');
				   		endwhile;
				   	endif;

				   	?>

					</div>
					<p>Complete your details here to download the document. By completing this form you may also receive business news and occasional marketing information from which you can unsubscribe at any time.</p>
				</div>
				<div class="col col1of2 toolkit-form">
					<?php echo do_shortcode('[formidable id=7]'); ?>
				</div>
			</article>

			<div class="toolkit-complete">
				<h3>Thank you.</h3>
				<p class="thankyou-text">We’ve sent an email to the address you provided. Please open your email and click on the download link.</p>
				<p class="thankyou-text-small">If you can’t see an email from Freeman Clarke in your inbox please check your spam folder or get in touch at <a href="tel:02030201864">0203&nbsp;020&nbsp;1864.</a></p>
			</div>

		</div>

	</div>
</section>

<?php get_footer(); ?>