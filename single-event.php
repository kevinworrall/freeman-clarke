<?php 
get_header(); 
if (have_posts()):
	while (have_posts()):
		the_post();
		$location = get_field('location');
		?>

		<section class="section event-section">

			<header class="event-header" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>)">
				<div class="container">
					<div class="content">
						<h1><?php the_title(); ?></h1>
						<h3><?php echo '<strong>'.get_field('start_time').'</strong>'; 
						if(get_field('end_time')) echo ' until <strong>'.get_field('end_time').'</strong>';
						?> | <?php echo $location['address']; ?></h3>
						<?php if(get_field('event_email_address')): ?>
						<a href="mailto:<?php the_field('event_email_address'); ?>" class="button">
							Enquire about this event
							<?php echo file_get_contents(get_theme_image('chevron-icon.svg')); ?>
						</a>
						<?php endif; ?>
					</div>
				</div>
			</header>

			<div class="container">
				
				<div class="main-content">
					<?php the_content(); ?>

					<?php if(get_field('event_email_address')): ?>
						<h3>Enquire about this event</h3>
						<p>For more information and to apply to attend, please contact <a href="mailto:<?php the_field('event_email_address'); ?>"><?php the_field('event_email_address'); ?></a></p>
						<?php endif; ?>
				</div>
				<?php get_sidebar('single-event'); ?>

			</div>
		</section>

		<?php
		$date_now = date('Y-m-d H:i:s');

		$args = array(
			'posts_per_page'	=> 4,
			'post_type'			=> 'event',
			'post__not_in' => array( get_the_ID() ),
			'order' => 'DESC',
	    'orderby' => 'meta_value',
	    'meta_key' => 'start_time',
	    'meta_type'			=> 'DATETIME'
		);
		
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ):
		?>
		<section class="section single-service-section">
			<div class="container">
				<h2>Other events</h2>
				<div class="tiles">
					<?php
						while ( $the_query->have_posts() ):
							$the_query->the_post();
							include(locate_template('inc/sections/tiles/event-tile.php'));
						endwhile;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</section>
		<?php endif; ?>

		<?php 
	endwhile;
endif;
get_footer(); ?>