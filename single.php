<?php 
get_header(); 
if (have_posts()):
	while (have_posts()):
		the_post();
		?>

		<section class="section business-insights-section">
			<div class="container">
				
				<div class="main-content">
					<h1><?php the_title(); ?></h1>
					<?php if ( ! post_password_required() ) : ?>
					<aside class="bi-post-meta">
						<div class="bi-post-info">
							<p><?php the_date(); ?> | <?php echo get_the_term_list(get_the_ID(),'category','',', '); ?></p>
						</div>
						<div class="bi-post-author">
							<?php
							$author_id = get_the_author_meta('ID');
							$author_slug = get_the_author_meta('display_name');


							$args = array(
							  'name'        => $author_slug,
							  'post_type'   => 'team',
							  'post_status' => 'publish',
							  'numberposts' => 1
							);
							switch_to_blog(1);
							$my_posts = get_posts($args);
							if( $my_posts ) :
							  $team_photo = get_field('photo', $my_posts[0]->ID );
							endif;
							$linkedin = get_field('linkedin_profile', $my_posts[0]->ID );
							restore_current_blog();
							?>
							<div class="team-photo" style="background-image: url(<?php echo $team_photo['sizes']['thumbnail']; ?>)"></div>
							<div class="author-info">
								<p>By <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author"><?php the_author(); ?></a></p>
							</div>

							<?php
							if($linkedin):
							?>
							<div class="linkedin">
								<a href="<?php echo $linkedin; ?>" target="_BLANK">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"/></svg>
								View my profile</a>
							</div>
							<?php endif; ?>
						</div>
						<div class="bi-post-share">
							<a target="_BLANK" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" class="linkedin">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0v24h24v-24h-24zm8 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.397-2.586 7-2.777 7 2.476v6.759z"/></svg>
							</a>
							<a target="_BLANK" href="https://twitter.com/home?status=<?php the_permalink(); ?>" class="twitter">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0v24h24v-24h-24zm18.862 9.237c.208 4.617-3.235 9.765-9.33 9.765-1.854 0-3.579-.543-5.032-1.475 1.742.205 3.48-.278 4.86-1.359-1.437-.027-2.649-.976-3.066-2.28.515.098 1.021.069 1.482-.056-1.579-.317-2.668-1.739-2.633-3.26.442.246.949.394 1.486.411-1.461-.977-1.875-2.907-1.016-4.383 1.619 1.986 4.038 3.293 6.766 3.43-.479-2.053 1.079-4.03 3.198-4.03.944 0 1.797.398 2.396 1.037.748-.147 1.451-.42 2.085-.796-.245.767-.766 1.41-1.443 1.816.664-.08 1.297-.256 1.885-.517-.44.656-.997 1.234-1.638 1.697z"/></svg>
							</a>
							<a href="mailto:?subject=<?php the_title(); ?>&body=I'd like to share this article with you: <?php the_permalink(); ?>" class="email">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm6.623 7.929l-4.623 5.712v-9.458l4.623 3.746zm-4.141-5.929h19.035l-9.517 7.713-9.518-7.713zm5.694 7.188l3.824 3.099 3.83-3.104 5.612 6.817h-18.779l5.513-6.812zm9.208-1.264l4.616-3.741v9.348l-4.616-5.607z"/></svg>
							</a>
							<!-- <a href="#" class="share">
							</a> -->
						</div>
					</aside>
					<?php 
					endif;

					$video = get_field('video');
					$image = get_the_post_thumbnail_url(get_the_ID(),'large');

					if($video): ?>
						<div class="post-featured-video">
							<div class="video-player">
								<?php the_field('video'); ?>
							</div>
							<div class="cover" style="background-image: url(<?= $image; ?>)">
								<a href="#" class="play-button">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-3 17v-10l9 5.146-9 4.854z"/></svg>
								</a>
							</div>
						</div>
					<?php else: ?>
						<div class="post-featured-image" style="background-image: url(<?= $image; ?>)"></div>
					<?php
					endif;
					the_content();
					get_sections();
					?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</section>

		<?php 
    if($_GET['utm_source'] !== 'GatorMail'): ?>
		<div class="lightbox">
			<div class="popup" data-popup="pdf-download">
				<a href="#" class="close-button">
					<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 24 24"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg>
				</a>
				<header class="popup-header">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 13h-5v5h-2v-5h-5v-2h5v-5h2v5h5v2z"></path></svg>
					<h2>Business insights</h2>
				</header>
				<div class="content">
					<?php echo do_shortcode('[formidable id=pkzvj]'); ?>
				</div>
				<div class="visible-only-when-sent">
					<h3>Thank you.</h3>
					<h4>We’ve sent an email to the address you provided. Please open your email and click on the download link.</h4>
					If you can’t see an email from Freeman Clarke in your inbox please check your spam folder or get in touch at <a href="tel:02030201864">0203&nbsp;020&nbsp;1864.</a>
				</div>
			</div>
		</div>
		<?php 
		endif;
	endwhile;
endif;
get_footer(); ?>