<?php
get_header(); ?>
<section class="section header-section bg-black">
  <div class="container">
    <h1>Business insights from <?php echo get_the_author_meta( 'display_name' ); ?></h1>
  </div>
</section>
<section class="section tiles-section" data-tiles-per-row="2">
  <div class="container">
    <div class="main-content tiles">
      <?php
      if (have_posts()):
        while (have_posts()):
          the_post();
          if(get_post_type() == 'post')
            include(locate_template('inc/sections/tiles/business-insights-tile.php'));
          elseif(get_post_type() == 'stories')
            include(locate_template('inc/sections/tiles/client-stories-tile.php'));
          elseif(get_post_type() == 'service')
            include(locate_template('inc/sections/tiles/services-tile.php'));
          elseif(get_post_type() == 'event')
            include(locate_template('inc/sections/tiles/event-tile.php'));
          elseif(get_post_type() == 'team')
            include(locate_template('inc/sections/tiles/team-tile.php'));
        endwhile;
      endif;
      ?>
      <div class="pagination">
        <?php echo paginate_links(); ?>
      </div>
    </div>
    <?php get_sidebar(); ?>
  </div>
</section>
<?php get_footer(); ?>