<?php

/**
 * Includes setup scripts
 */
include_once('inc/post-types.php');         // Include all Post Types
include_once('inc/taxonomies.php');         // Include all Taxonomies
include_once('inc/acf-fields.php');         // Include all ACF Fields
include_once('inc/widgets.php');            // Include our Widgets
include_once('inc/toolkit_functions.php');  // Include our Toolkit functions

/**
 * Enqueues all scripts & styles
 */
function fc_enqueue_scripts() {
  wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800');
  wp_enqueue_script('googlemap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAELEhgV7Ozjnz-XNp0ifaJ8FSyQAwU7IA');
  wp_enqueue_script('adroll-script', get_stylesheet_directory_uri() . '/js/adrollscript.js');
}
add_action( 'wp_enqueue_scripts', 'fc_enqueue_scripts' );

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_post_type_support( 'page', 'excerpt' );
add_filter( 'widget_text', 'do_shortcode' );


/**
 * Setup theme settings (Images sizes, Supports)
 */
function fc_theme_setup() {
  add_image_size( 'carousel-image', 2500, 1200, false );
  add_image_size( 'tile-image', 400, 300, false );

}
add_action( 'after_setup_theme', 'fc_theme_setup' );


/**
 * Allows for SVGs to be uploaded
 */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Prints the URL for a Theme Folder Image
 */
function theme_image($file) {
  echo get_theme_image($file);
}

/**
 * Returns the URL for a Theme Folder Image
 */
function get_theme_image($file) {
  return get_stylesheet_directory_uri() . '/images/' . $file;
}

/**
 * Print out each Page Section
 */
function get_sections() {
  global $post;
  // if (have_posts()): 
  //   while (have_posts()):
  //     the_post();

      if($post->post_parent): 
        $parent_post = get_post($post->post_parent);
        $parent_post_title = $parent_post->post_title;

        if(get_field('display_title')):
        ?>
        <section class="section header-section bg-black">
          <div class="container">
            <h1><?php echo $parent_post_title; ?></h1>
          </div>
        </section>
        <?php endif; ?>
        <section class="section">
          <div class="container">
            <div class="main-content">
        <?php
      endif;

      if (have_rows('sections')):
        while (have_rows('sections')):
          the_row();
          $layout = get_row_layout();
          get_template_part('inc/sections/'.$layout.'');
        endwhile;
      else:
        get_template_part('inc/sections/content');
      endif;

      if($post->post_parent){
        echo '</div>';
        get_sidebar('page');
        echo '</div></section>';
      }

  //   endwhile;
  // endif;
}

/**
 * Adds the current website ID to the body class
 */
function custom_body_class( $classes ) {
  global $current_blog;
  $classes[] = 'website-'.$current_blog->blog_id;
  return $classes;
}
add_filter( 'body_class', 'custom_body_class' );

/**
 * Add AFC Options Pages
 */
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Site Options',
    'menu_title'  => 'Site Options',
    'menu_slug'   => 'site-options',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}

/**
 * Rename 'Posts' to 'Business Insights'
 */
function fc_change_post_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'Business Insights';
  $submenu['edit.php'][5][0] = 'Business Insights';
  $submenu['edit.php'][10][0] = 'Add Business Insight';
  $submenu['edit.php'][16][0] = 'Tags';
}
add_action( 'admin_menu', 'fc_change_post_label' );

/**
 * Rename all labels for posts
 */
function fc_change_post_object() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'Business Insights';
  $labels->singular_name = 'Business Insight';
  $labels->add_new = 'Add Business Insight';
  $labels->add_new_item = 'Add Business Insight';
  $labels->edit_item = 'Edit Business Insight';
  $labels->new_item = 'Business Insights';
  $labels->view_item = 'View Business Insight';
  $labels->search_items = 'Search Business Insights';
  $labels->not_found = 'No Business Insights found';
  $labels->not_found_in_trash = 'No Business Insights found in Trash';
  $labels->all_items = 'All Business Insights';
  $labels->menu_name = 'Business Insights';
  $labels->name_admin_bar = 'Business Insights';
}
add_action( 'init', 'fc_change_post_object' );

/**
 * Remove Metaboxes
 */
function fc_remove_metaboxes(){
    remove_meta_box( 'tagsdiv-stories_cat' , 'stories' , 'side' ); 
    remove_meta_box( 'postimagediv' , 'stories' , 'side' ); 
}
add_action('do_meta_boxes', 'fc_remove_metaboxes');


/**
 * Set Team Member Featured Image
 */
function acf_set_featured_image( $value, $post_id, $field  ){
    if($value != ''){
      add_post_meta($post_id, '_thumbnail_id', $value);
      update_post_meta($post_id, '_thumbnail_id', $value);
    }
    return $value;
}
add_filter('acf/update_value/name=photo', 'acf_set_featured_image', 10, 3);
add_filter('acf/update_value/name=featured_image', 'acf_set_featured_image', 11, 3);

/**
 * Print Section Background Colour
 */
function fc_section_background_colour() {
  $bg_colour = get_sub_field('background_colour');
  if($bg_colour) echo 'bg-'.$bg_colour;
}



// add_filter('query_vars', 'my_register_query_vars' );
// function my_register_query_vars( $qvars ){
//     //Add query variable to $qvars array
//     $qvars[] = 'utm_source';
//     return $qvars;
// }



/**
 * Print the download link on service
 */
function fc_download_link() {

  if (have_rows('downloads')):
    while (have_rows('downloads')):
      the_row();
      $image = get_sub_field('image');
      $title = get_sub_field('title');
      $desc = get_sub_field('description');
      $file = get_sub_field('file');
      $icon = file_get_contents(get_theme_image('chevron-icon.svg'));
    endwhile;
    if($_GET['utm_source'] !== 'GatorMail'):
return <<<HTML
  <div class="download-link">
    <div class="image">
      <img src="{$image['sizes']['large']}"/>
    </div>
    <div class="content">
      <h2>{$title}</h2>
      <p>{$desc}</p>
      <a class="button trigger-business-insights" target="_BLANK" data-href="{$file['url']}">
        Download now for FREE!
        {$icon}
      </a>
    </div>
  </div>
HTML;
    else:
return <<<HTML
  <div class="download-link">
    <div class="image">
      <img src="{$image['sizes']['large']}"/>
    </div>
    <div class="content">
      <h2>{$title}</h2>
      <p>{$desc}</p>
      <a class="button trigger-business-insights" target="_BLANK" href="{$file['url']}">
        Download now for FREE!
        {$icon}
      </a>
    </div>
  </div>
HTML;
    endif;
  endif;
}
add_shortcode( 'download-link', 'fc_download_link' );


/**
 * Print the footer widgets
 */
function fc_footer_widgets() {
  $count = 4;
  for ($i=1; $i < $count; $i++) { 
    $name = 'footer_column_'.$i;
    if ( is_active_sidebar( $name ) ):
      echo '<div class="col">';
      dynamic_sidebar( $name );
      echo '</div>';
    endif;
  }
}

/**
 * Reorders Archive Posts
 */
function my_pre_get_posts( $query ) {

  if( is_admin() ) {
    return $query;
  }
  
  if(is_home()):
    $query->set('orderby', 'post_date');
    $query->set('order', 'DESC');
  endif;
  
  return $query;
}

add_action('pre_get_posts', 'my_pre_get_posts');



/**
 * Change the output of Archive Titles
 */
function fc_archive_title( $title ) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
    $title = single_tag_title( '', false );
  } elseif ( is_author() ) {
    $title = '<span class="vcard">' . get_the_author() . '</span>';
  } elseif ( is_post_type_archive() ) {
    $title = post_type_archive_title( '', false );
  } elseif ( is_tax() ) {
    $title = single_term_title( '', false );
  }
  return $title;
}
 
add_filter( 'get_the_archive_title', 'fc_archive_title' );


/**
 * Update ACF Google Map API
 */
function fc_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyAELEhgV7Ozjnz-XNp0ifaJ8FSyQAwU7IA';
  return $api;
} 
add_filter('acf/fields/google_map/api', 'fc_acf_google_map_api');


/**
 * Move YoastSEO metabox to the bottom
 */
function fc_yoasttobottom() {
  return 'low';
}

add_filter( 'wpseo_metabox_prio', 'fc_yoasttobottom');





// function add_custom_caps() {
//     // gets the subscriber role
//     $role = get_role( 'events_editor' );

//     print_r($role); die();

//     // This only works, because it accesses the class instance.
//     // would allow the subscriber to edit others' posts for current theme only
//      $role->add_cap( 'read' );
//      $role->add_cap( 'read_post');
//      $role->add_cap( 'read_private_post' );
//      $role->add_cap( 'edit_post' );
//      $role->add_cap( 'edit_others_post' );
//      $role->add_cap( 'edit_published_post' );
//      $role->add_cap( 'publish_post' );
//      $role->add_cap( 'delete_others_post' );
//      $role->add_cap( 'delete_private_post' );
//      $role->add_cap( 'delete_published_post' );
// }
// add_action( 'admin_init', 'add_custom_caps');



add_action('admin_init','psp_add_role_caps',999);
function psp_add_role_caps() {

  // Add the roles you'd like to administer the custom post types
  $roles = array('events_editor');
  
  // Loop through each role and assign capabilities
  foreach($roles as $the_role) { 

       $role = get_role($the_role);
       $role->add_cap( 'read' );
       $role->add_cap( 'read_event');
       $role->add_cap( 'read_private_events' );
       $role->add_cap( 'edit_event' );
       $role->add_cap( 'edit_events' );
       $role->add_cap( 'edit_others_events' );
       $role->add_cap( 'edit_published_events' );
       $role->add_cap( 'publish_events' );
       $role->add_cap( 'delete_others_events' );
       $role->add_cap( 'delete_private_events' );
       $role->add_cap( 'delete_published_events' );

  }
}



add_filter( 'tiny_mce_before_init', 'fb_tinymce_add_pre' );
function fb_tinymce_add_pre( $initArray ) {

    // Comma separated string of extended tags
    // Command separated string of extended elements
    $ext = 'svg[preserveAspectRatio|style|version|viewbox|xmlns],defs,linearGradient[id|x1|y1|z1]';

    if ( isset( $initArray['extended_valid_elements'] ) ) {
        $initArray['extended_valid_elements'] .= ',' . $ext;
    } else {
        $initArray['extended_valid_elements'] = $ext;
    }
    // maybe; set tiny paramter verify_html
    //$initArray['verify_html'] = false;

    return $initArray;
}



function fc_button_shortcode( $atts ) {
  if(!$atts['text']) $atts['text'] = 'Read more';

  return '
    <a href="'.$atts['url'].'" class="button">
      '.$atts['text'].'
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"></path></svg>
    </a>';
}
  
add_shortcode( 'button', 'fc_button_shortcode' );
