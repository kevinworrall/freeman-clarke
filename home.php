<?php get_header(); ?>
<section class="section header-section bg-black">
	<div class="container">
		<h1>Business insights</h1>
	</div>
</section>
<section class="section single-service-section">
	<div class="container">
		<div class="main-content tiles">
			<?php
			if (have_posts()):
				while (have_posts()):
					the_post();
					include(locate_template('inc/sections/tiles/business-insights-tile.php'));
				endwhile;
			endif;
			?>
			<div class="pagination">
				<?php echo paginate_links(); ?>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>
<?php get_footer(); ?>