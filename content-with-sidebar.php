<?php 
/* Template Name: Content only (custom sidebar) */
get_header(); 
if (have_posts()):
	while (have_posts()):
		the_post();
		?>
		<section class="section business-insights-section">
			<div class="container">
				<div class="main-content">
					<?php the_content(); ?>
				</div>
				
				<aside class="sidebar">
					<?php
					$direct_parent = $post->post_parent;
					$args = array(
					    'post_type'      => 'page',
					    'posts_per_page' => -1,
					    'post_parent'    => $direct_parent, 
					    'order'          => 'ASC',
					    'orderby'        => 'menu_order',
					    'post__not_in' => array( $post->ID ),
					 );
					$parent = new WP_Query( $args );
					if ( $parent->have_posts() ) : ?>
					 
					<div class="business-insights">
					 <ul>  
					   <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			        <li>
				        <a href="<?php the_permalink()?>" title="<?php the_title(); ?>">
		              <span class="thumbnail" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)"></span>
		              <span class="title"><?php the_title(); ?><p><?php echo wp_trim_words( get_the_content(), 15, '...' ); ?></p></span>
			          </a>
		          </li>
					  <?php endwhile; ?>
		       	</ul>
					</div>

					<?php 
				endif; 
				wp_reset_query();
				?>
				</aside>

			</div>
		</section>
		<?php 
	endwhile;
endif;
get_footer(); ?>